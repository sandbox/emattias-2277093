<?php

function _partials_settings(){

  $features_options = array();
  foreach(features_get_features() as $feature_info){
    $features_options[$feature_info->name] = $feature_info->info['name'];
  }

  $default_value = array();
  if(($partials_config = variable_get('partials_config')) && !empty($partials_config['module'])){
    $default_value = array_keys($partials_config['module']);
  };

  $form['features'] = array(
    '#title' => t('Features'),
    '#type' => 'checkboxes',
    '#options' => $features_options,
    '#default_value' => $default_value,
    '#description' => t('Choose which custom feature modules you want to enable to have partials. Note that if enabled here partial hooks will be triggered even though the feature is disabled! (for performance reasons).')
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}

function _partials_settings_submit(&$form, &$form_state){
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  $partials_config = array();
  foreach($form_state['values']['features'] as $machine_name => $status){
    if($status){
      $partials_config['module'][$machine_name] = array();
    }
  }

  variable_set('partials_config', $partials_config);

  // Reset module_implements cache
  module_implements('', FALSE, TRUE);
}
